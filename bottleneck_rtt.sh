#!/bin/bash
username=$(<USERNAME.txt)
readarray -t cip < ./CIP.txt
readarray -t cname < ./CNAME.txt
readarray -t cport < ./CPORT.txt

i=0
for clientip in ${cip[@]}
do
	ssh $username@${cname[i]} -p ${cport[i]} "sudo tc qdisc replace dev eth1 root netem delay 100ms" &
	i=$(($i+1))
done


