#!/bin/sh
#upload to each server
#1st column of the output file : time(sec), actually it's icmp_seq
#2nd column of the output file : rtt in ms (?one way delay?)

destip=$1 # client's IP address
pingcount=$2
#sudo ping -i 0.1 -c $pingcount $destip | tee originalping.csv 
sudo ping -i 0.1 -c $pingcount $destip > originalping.csv 


tail -n+2 originalping.csv | head -n-4 > middleping.csv
awk -F "[: ]" '{print $6","$8}' middleping.csv | sed -e "s/icmp_seq=/ /" | sed -e "s/time=/ /" > ping_delay.csv
#sudo rm originalping.csv
#sudo rm middleping.csv



