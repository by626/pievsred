clear;
clc;
clf;
rtt=csvread('rtt.csv');%rtt before setting queue discipline
pie1=csvread('piet1.csv');
pie2=csvread('piet2.csv');
pie3=csvread('piet3.csv');

t1=pie1(:,1)/10;
d1=pie1(:,2)-rtt(1);
t2=pie2(:,1)/10;
d2=pie2(:,2)-rtt(2);
t3=pie3(:,1)/10;
d3=pie3(:,2)-rtt(3);

figure(1);
plot(t1,d1,t2,d2,t3,d3);
legend('server1','server2','server3');
title('Queueing delay in PIE queue');
xlabel('experiment time (s)');
ylabel('queueing delay (ms)');
grid on;
ylim([0 350]);
xlim([0 250]);

set(findall(gcf,'type','text'),'fontSize',14);
