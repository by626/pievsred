clear;
clc;
clf;

rtt=csvread('rtt.csv');%rtt before setting queue discipline
red1=csvread('redt1.csv');
red2=csvread('redt2.csv');
red3=csvread('redt3.csv');

t1=red1(:,1)/10;
d1=red1(:,2)-rtt(1);
t2=red2(:,1)/10;
d2=red2(:,2)-rtt(2);
t3=red3(:,1)/10;
d3=red3(:,2)-rtt(3);

figure(1);
plot(t1,d1,t2,d2,t3,d3);
legend('server1','server2','server3');
title('Queueing delay in RED queue');
xlabel('experiment time (s)');
ylabel('queueing delay (ms)');
grid on;
ylim([0 1600]);
xlim([0 250]);

set(findall(gcf,'type','text'),'fontSize',14);