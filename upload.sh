#!/bin/bash
#upload scripts to all VMs
username=$(<USERNAME.txt)
readarray -t router < ./ROUTER.txt #read router's port number and hostname
readarray -t cname < ./CNAME.txt
readarray -t sname < ./SNAME.txt
readarray -t cport < ./CPORT.txt
readarray -t sport < ./SPORT.txt

scp -P ${router[1]} ./tc.sh $username@${router[0]}:./
scp -P ${router[1]} ./tc_modified.sh $username@${router[0]}:./
scp -P ${router[1]} ./queuemonitor.sh $username@${router[0]}:./

i=0
while [ $i -lt ${#sname[@]} ]
do
	scp -P ${sport[$i]} ./ping_delay.sh $username@${sname[$i]}:./
	scp -P ${sport[$i]} ./cp.sh $username@${sname[$i]}:./
	i=$(($i+1))
done

i=0
while [ $i -lt ${#cname[@]} ]
do
#	scp -P ${cport[$i]} ./autoftp.sh $username@${cname[$i]}:./
	scp -P ${cport[$i]} ./multi.sh $username@${cname[$i]}:./
	i=$(($i+1))
done
