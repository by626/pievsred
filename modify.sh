#!/bin/bash

#modify MTU, TCP configuration on servers

username=$(<USERNAME.txt)
#next input arguments: 3 servers' interfaces
readarray -t sname < ./SNAME.txt
readarray -t sport < ./SPORT.txt

#modify MTU on servers
#MTU = (packet size 1000 Bytes) - (EthernetHeader 14 Bytes) - (FCS 4 Bytes) = 982

i=0
for sintf in "$@"
do
	ssh $username@${sname[$i]} -p ${sport[$i]} "sudo ifconfig $sintf mtu 982" 
	i=$(($i+1))
done
echo "MTU modified"

#configuring TCP on servers
i=0
while [ $i -lt ${#sname[@]} ]
do
	ssh $username@${sname[$i]} -p ${sport[$i]} 'echo 1 | sudo tee /proc/sys/net/ipv4/tcp_sack'
	ssh $username@${sname[$i]} -p ${sport[$i]} 'echo reno | sudo tee /proc/sys/net/ipv4/tcp_congestion_control'
	i=$(($i+1))
done
echo "TCP sack, new reno, done"

