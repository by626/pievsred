#!/bin/bash

username=$(<USERNAME.txt)
qdisc=$1 #pie or red
rinf_out=$2 #interface with pie/red

readarray -t router < ./ROUTER.txt #read router's port number and hostname
readarray -t cip < ./CIP.txt
readarray -t sip < ./SIP.txt
readarray -t cname < ./CNAME.txt
readarray -t sname < ./SNAME.txt
readarray -t cport < ./CPORT.txt
readarray -t sport < ./SPORT.txt

#start vsftpd on servers
i=0
while [ $i -lt ${#sname[@]} ]
do
	ssh $username@${sname[$i]} -p ${sport[$i]} 'sudo start vsftpd'
	i=$(($i+1))
done

#recording data
i=0
for clientip in ${cip[@]}
do
	ssh $username@${sname[i]} -p ${sport[i]} "./ping_delay.sh $clientip 2600" &
	i=$(($i+1))
done

if [ $qdisc = "pie" ]; then
	ssh $username@${router[0]} -p ${router[1]} "./queuemonitor.sh $rinf_out 250 0.1 > router_pie.txt" &
else
	ssh $username@${router[0]} -p ${router[1]} "./queuemonitor.sh $rinf_out 250 0.1 > router_red.txt" &
fi

#clients get files
ssh $username@${cname[0]} -X -p ${cport[0]} "./multi.sh ${sip[0]} 250 test1.rmvb test2.rmvb test3.rmvb test4.rmvb test5.rmvb test6.rmvb test7.rmvb test8.rmvb test9.rmvb test10.rmvb" &

sleep 50
ssh $username@${cname[1]} -X -p ${cport[1]} "./multi.sh ${sip[1]} 150 test1.rmvb test2.rmvb test3.rmvb test4.rmvb test5.rmvb test6.rmvb test7.rmvb test8.rmvb test9.rmvb test10.rmvb test11.rmvb test12.rmvb test13.rmvb test14.rmvb test15.rmvb test16.rmvb test17.rmvb test18.rvb test19.rmvb test20.rmvb" &

sleep 50
ssh $username@${cname[2]} -X -p ${cport[2]} "./multi.sh ${sip[2]} 50 test1.rmvb test2.rmvb test3.rmvb test4.rmvb test5.rmvb test6.rmvb test7.rmvb test8.rmvb test9.rmvb test10.rmvb test11.rmvb test12.rmvb test13.rmvb test14.rmvb test15.rmvb test16.rmvb test17.rmvb test18.rvb test19.rmvb test20.rmvb" &

sleep 160
#stop vsftpd on servers
i=0
while [ $i -lt ${#sname[@]} ]
do
	ssh $username@${sname[$i]} -p ${sport[$i]} 'sudo stop vsftpd'
	i=$(($i+1))
done
echo "servers stopped"





