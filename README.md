##Introduction
My experiment compares the queueing delay when setting the PIE and RED queue disciplines in GENI. It will take about one hour to run my experiment and get the figures. The original simulation results are presented by *Rong Pan P. Natarajan, C. Piglione,M.S. Prabhu, V. Subramanian, F. Baker, and B. VerSteeg. "PIE: A lightweight control scheme to address the bufferbloat problem." In 2013 IEEE 14th International Conference on High Performance Switching and Routing (HPSR). 8-11 July 2013*. 
##Background
Many active queue management schemes are designed to control the bufferbloat problems. It is necessary to compare their performance under some situations in order to achieve our specific goals like low latency and high link utilization. PIE(Proportional Integral controller Enhanced) is a new scheme that can help get very low queueing delay compared with the RED(Random Early Detection).
##Results
The orginal figure from the paper:(Fig. 8. PIE vs. RED Performance Comparison Under Varying Traffic Intensity: 0s-50s, traffic load is 10 TCP flows; 50s-100s, traffic load is 30 TCP flows;100s-150s, traffic load is increased to 50 TCP flows; traffic load is then reduced to 30 and 10 at 200s and 250s respectively. Due to static configured parameters, the queueing delay increases under RED as the traffic intensifies. The autotuning feature of PIE, however, allows the scheme to control the queueing latency quickly and effectively.)
![Imgur](http://i.imgur.com/XVhepRl.png)

My results(I put the delay of two schemes separately):

(1).Queueing delay when PIE is implemented

![pie_10M_no](http://i.imgur.com/Y0FEGWp.png)

(2).Queueing delay when RED is implemented

![red_10M_no](http://i.imgur.com/Icc3FZh.png)

My plots show that the queueing delay when PIE is implemented is quite close to the original result, which keeps a low value during the whole experiment period and fluctuate within 0-50ms. The delay when RED is implemented, however, is much longer than the PIE situation. We could see the slightly difference between the value of queueing delay when traffic intensity changes, although this difference is not as obvious as the orginal results did.

Therefore, the only result I got which is the same with the original result is that the queueing delay in RED queue is quite larger than that in PIE queue. I have to say that the trend of my result is not the same with the original result. The value, especially when RED is implemented is quite larger. I got the queueing delay at around 350ms to 420ms while the original shows a value less than 150ms. What's more, the remarkable change of queueing delay in RED queue when traffic intensity varies can not be seen in my plot. Instead, my plot shows a “flat” trend of the queueing delay when traffic intensity changes.

##Run my experiment
###A.Some preparations

####1.Get materials.
Open a terminal and run
	
	git clone https://bitbucket.org/by626/pievsred.git
	
to get my scripts and some .txt files as examples. Go to the directory of **./pievsred** then continue with the  following steps.

####2.A new slice
Create a new slice on [GENI](https://www.geni.net) and click the "Add Resources" button. Then upload my RSpec(final\_by626\_request\_rspec.xml, which is downloaded in step 1 of section A) to get the same topology as I did. You should have the topology like
![Imgur](http://i.imgur.com/Fzyrzsy.png)

####3.Get VMs' information and upload scripts to VMs
Then you'll specify an aggregate and wait for tens of seconds to reserve resources. Once you could successfully reserve resources, get the details of each VM such as IP address, host name and port number. You should also log into each server and the router to find the interface corresponding to the IP address. Create 8 files with suffix .txt to store those informations. The file names and contents are:

File Name    | Content
------------ | --------
USERNAME.txt | only one line: your username
CIP.txt      | each line has one client's IP address
SIP.txt      | each line has one server's IP address
CPORT.txt    | each line has one client's port number
SPORT.txt    | each line has one server's port number
CNAME.txt    | each line has one client's hostname
SNAME.txt    | each line has one server's hostname
ROUTER.txt   | the first line is router's hostname and the second line is router's port numberline is router's hostname and the second line is router's port number

You should be careful that the same line in CIP.txt, CPORT.txt, CNAME.txt corresponds to the **same** client and the same line in SIP.txt, SPORT.txt, SNAME.txt corresponds to the **same** server. (In my situation,  the information for server 3 are all at the third line on SIP.txt, SPORT.txt and SNAME.txt) 

####4.Prepare for the experiment
#####Iproute2
Log in to the router, type `ip -V` to see the current version, you may see the output like "ip utility, iproute2-ss131122".

You can check the version number and the corresponding date [here](https://www.kernel.org/pub/linux/utils/net/iproute2/). If you get the version older than me then you need to upgrade your iproute2 version. However if you get the same(iproute2 3.14.0) or later version, you can go directly to step 3 of section A.

Go to [this link](https://launchpad.net/ubuntu/+source/iproute2/3.14.0-1) and choose the corresponding version for your own situation. You can type `uname -m` on terminal to check the machine hardware. If you see "x86_64" you need to choose *amd64* under "Builds", or you'll choose *i386* wihch refers to the 32-bit edition.

After you click the *amd64* or *i386*, you could find the Build files on the new page. Right click that file and choose **Copy Link Location**(you may get slighty different in your situation, but always choose the one which has the same meaning with "copy link location"). Then on terminal (on router), type

	wget <Link>
	
where < Link > is the link you just copied.

Type
		
	sudo dpkg -i iproute2_3.14.0-1_amd64.deb

You may need to change it to

	sudo dpkg -i iproute2_3.14.0-1_i386.deb 
	
if you need a 32-bit edition.

Run

	sudo apt-get update
	sudo apt-get install linux-generic-lts-vivid
	
to get the new kernel.

Finally, run

	sudo /sbin/reboot
	
to reboot the system.

#####Upload scripts to all VMs
On your local machine, run 

	./upload.sh
This will upload each bash script to the corresponding remote VM.

#####vsftpd(This should be done on three servers)
Log into one server, run

	sudo apt-get update
	sudo apt-get install vsftpd
to install vsftpd. After you finish that, on your local machine, run 

	scp -P <port> ./vsftpd.conf <username>@<hostname>:./
where < port > is server's port number and <hostname> is server's hostname. Then on server, run

	sudo cp vsftpd.conf /etc/
to copy the config file for vsftpd to the right location so that anonymous ftp will be allowed. 

Then on your local machine, run 

	scp -P <port> ./test.rmvb <username>@<hostname>:./
to copy the files to the server( each file is 253MB so this may take several minutes). Here the < port > and the < hostname > also mean the port number and hostname of that server. Then on server, run

	./cp.sh
This will create twenty files and put them into the default FTP directory to let clients get those files. (This will also take several miniutes. After it's done you can run `ls /srv/ftp/` to check that all the files are in that location.)

On the other two servers, do the same thing.


###B.Experiment starts !
####1.Get RTT before setting the queue discipline
In order to set the bottleneck rtt, run
	
	./bottleneck_rtt.sh
on your local machine to set delay=100ms on 3 clients.

Then still on your local machine, run

	./pingtest.sh
and after 30 seconds, you will see the average rtt measured by each server. The value will also be automatically written to a .txt file with the name of each client's IP address. (Don't worry if you see *"rm: cannot remove ‘rtt.csv’: No such file or directory"*. The file will be created later.)

####2.Some configurations
To get the configuration the same with what the paper said, on your local machine run

	./modify.sh
	
The output means that you've already set the TCP New Reno(The “Reno” congestion control provided by the Linux kernel is actually the  New Reno algorithm) with SACK on each server.

####3.Set queue discipline on router
On router, run

	./tc.sh <qdisc> <interface>
	
where < qdisc > should be **pie**, which is the queue descipline we are testing and < interface > is the interface connected to the clients. The output tells you the current settings of the queue and you could ignore "RTNETLINK answers: No such file or directory" if you see that on the first line. Now you should see a new line *qdisc pie 10:*.
![Imgur](http://i.imgur.com/x3c1LyC.png)
####4.Start traffic
On your local machine, run

	./all.sh <qdisc> <interface>
	
where < interface > is the same interface where you set the queue and < qdisc > is the queue discipline(pie or red). This will first start running vsftpd on each server to let three clients retrieve files from them. It will last around 5 minutes and you should do nothing during this whole process. When you first run it you probabily see the error like **"/usr/bin/xauth:  file /users/by626/.Xauthority does not exist"**. If you see that, just wait for it ending itself and run all.sh again. Similarly, you can also ignore the message like **"Warning: time of day goes back (-1225680us), taking countermeasures."**
	
####5.Get data from VMs
After you see "servers stopped" which means you've finish your test, run

	./getdata.sh <qdisc>
on your local machine and < qdisc > should be the queue discipline the same as step 3 and 4 in section B. In both situations, you will get 3 files on your local machine.

####6.RED experiment
Change < qdisc > into **red** and then repeat step 3-4 in section B.
###Visulize the results
Some of the files you just got is used to plot the results in matlab so make sure those files (including **rtt.txt** you got in step 1 of section B) and the matlab scripts are at the same location.

I have two matlab script pie.m and red.m. They will show you the delay from ping (minus the delay before setting the queue). For `ylim([0 300])`,You may have some other settings based on your situation.


##Release resourses

After finish the experiment and if you do not need to check them anymore please release all the resourses.

On each client and the router, run

	sudo tc qdisc del dev <interface> root
	
where < interface > is the interface that you set something(pie, red, or netem) with tc.
##Notes
#####1. Information about my machine and the kernel
x86_64;
Linux ubuntu 3.19.0-58-generic #64~14.04.1-Ubuntu
	
#####2. GENI aggregate:
InstaGENI

#####3. Version of tools:
iproute2-ss131122(3.14.0) on remote VM, as mentioned above.
Matlab R2015a(8.5.0)
	
#####4. External sources:
bash script [queuemonitor.sh](http://witestlab.poly.edu/~ffund/el7353/2-testbed-mm1.html)
	
ubuntu manuals: [ftp](http://manpages.ubuntu.com/manpages/trusty/man1/tnftp.1.html), [pie](http://manpages.ubuntu.com/manpages/xenial/en/man8/tc-pie.8.html), [red](http://manpages.ubuntu.com/manpages/xenial/en/man8/tc-pie.8.html)
	
other documents: [htb manual](http://luxik.cdi.cz/~devik/qos/htb/manual/userg.htm), [Linux Advanced Routing & Traffic Control HOWTO](http://www.lartc.org/howto/)

#####5. Scale down or not
I only have 7 nodes I did not scale down the original experiment design.	

#####6.Some modifications
The "flat" result in RED queue is not the same with the orginal result. We could not see the remarkable difference between queueing delay when traffic intensity changes. Maybe the reason is that even a small number of flows already saturate the link and therefore, if we increase the number of flows, the actual increasement of traffic intensity will be limited. Therefore, I add a modified version of this experiment. On each server, run 

	sudo vi /etc/vsftpd.conf

and uncomment the last line **anon_max_rate = 25000**, which will limit the maximum data transfer rate to 25kB/s. If we have 50 sources limited by it, the maximum data rate is 10Mb/s, which is just the bottleneck link bandwidth. Below are the results of this modifed experiment.
	
![pie_10M_25000](http://i.imgur.com/ifOAj4K.png)
![red_10M_25000](http://i.imgur.com/EdCLTtl.png)

If we want to observe the remarkable change of queueing delay, we should let the traffic not saturate the bottleneck link. We could notice that the change of queueing delay in RED queue varies a lot when traffic intensity changes, which is the feture we expected before.
	
To summerize, when the data rate before entering the queue (in router) is less than the bandwidth of the link connected to the outgoing interface, we could see the remarkable change of queueing delay as traffic intensity changes, just like this modified version. However, if the incoming rate is greater than the outgoing rate, the queueing delay will not change that much even if traffic intensity keeps increasing.