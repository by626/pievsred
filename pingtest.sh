#!/bin/bash
#get rtt before the setting the qdisc

username=$(<USERNAME.txt)
readarray -t cip < ./CIP.txt
readarray -t sip < ./SIP.txt
readarray -t sname < ./SNAME.txt
readarray -t sport < ./SPORT.txt

rm rtt.csv

i=0
while [ $i -lt ${#sname[@]} ]
do
	ssh $username@${sname[$i]} -p ${sport[$i]} "ping -c 20 ${cip[$i]}" > ${cip[$i]}.csv &
	i=$(($i+1))
done

sleep 30

i=0
while [ $i -lt ${#sname[@]} ]
do
	echo "average rtt (${sip[$i]} ping ${cip[$i]}):"
	tail -1 ${cip[$i]}.csv | awk -F "[: ]" '{print $4}' | awk -F/ '{print $2}' | tee -a rtt.csv
	i=$(($i+1))
done


