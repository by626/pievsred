#!/bin/bash
username=$(<USERNAME.txt)
qdisc=$1 #pie or red
readarray -t router < ./ROUTER.txt #read router's port number and hostname
readarray -t cname < ./CNAME.txt
readarray -t sname < ./SNAME.txt
readarray -t cport < ./CPORT.txt 
readarray -t sport < ./SPORT.txt

if [ $qdisc = "pie" ]; then
#	scp -P ${router[1]} $username@${router[0]}:./router_pie.txt ./
#	awk -F "[: ]" '{print $1","$71"}' router_pie.txt | sed -e 's/us/ /' | sed -e 's/p/ /'> pie_delay.csv

	scp -P ${sport[0]} $username@${sname[0]}:./ping_delay.csv ./piet1.csv
	scp -P ${sport[1]} $username@${sname[1]}:./ping_delay.csv ./piet2.csv
	scp -P ${sport[2]} $username@${sname[2]}:./ping_delay.csv ./piet3.csv
else
#	scp -P ${router[1]} $username@${router[0]}:./router_red.txt ./

	scp -P ${sport[0]} $username@${sname[0]}:./ping_delay.csv ./redt1.csv
	scp -P ${sport[1]} $username@${sname[1]}:./ping_delay.csv ./redt2.csv
	scp -P ${sport[2]} $username@${sname[2]}:./ping_delay.csv ./redt3.csv

fi


