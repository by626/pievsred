#!/bin/sh
#upload to each ftp server

#open several terminals, get files from one server

addr=$1
time=$2

starttime=$(date '+%H:%M:%S');
echo "start getting files from" $addr "at" "$starttime"

shift 2
for filename in "$@"
do
#	xterm -e ./autoftp.sh $addr $filename &
	wget -q -O/dev/null ftp://$addr/$filename &
#	wget -q ftp://$addr/$filename &
done

sleep $time
#end=$(date +%s)

#PID=`ps -ef | grep ftp | grep -v "grep" | awk '{print $2}'`
#kill -9 $PID
sudo kill -9 `ps -ef | grep ftp | grep -v grep | awk '{print $2}'`

endtime=$(date '+%H:%M:%S');
echo "stop getting files from" $addr "at" "$endtime"


